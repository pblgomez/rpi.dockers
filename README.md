# rpi.dockers

A list of dockers for raspberry pi
Clone this at $HOME



## Traefik
Rename $HOME/dockersetups/env to $HOME/.env and replace variables
```sh
cp -b $HOME/dockersetups/env $HOME/.env
```

## Mosquitto
IMPORTANT! You need to execute:
$ touch passwd
$ mosquitto_passwd -b passwd USERNAME PASSWORD
on a system with mosquitto installed first and copy the file passwd to this directory.


## Node-red
IMPORTANT! You need to replace MY_PASSWORD in docker-setups/docker-configuration.sh
