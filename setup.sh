#!/usr/bin/env bash
read -p "Do you want to use a new docker-compose file? [yn] " answer
if [[ $answer = y ]] ; then
echo "Backing up old docker-compose.yaml just in case"
if [ -f $HOME/docker-compose.yaml ]; then
    cp $HOME/docker-compose.yaml $HOME/docker-compose.yaml.bak
fi
cat  > $HOME/docker-compose.yaml << EOF
version: '3.6'
services:
EOF
fi

if [ ! -f $HOME/.env ]; then
  cp $HOME/rpi.dockers/.env.sample $HOME/.env
fi



########################
# MOSQUITTO
########################
read -p "Do you want to install mosquitto? [yn] " answer
if [[ $answer = y ]] ; then
  echo "IMPORTANT! You need to execute:
  $ touch passwd
  $ mosquitto_passwd -b passwd USERNAME PASSWORD
  on a system with mosquitto installed first and copy the file passwd to this directory
  To copy:
    scp passwd pi@<IP_of_RaspberryPi>:/home/pi/
  Don't continue if you haven't done it. Crtl+c to cancel"
  read -p "Press enter to continue "
  echo "creating config folder"
  mkdir -p $HOME/config/mosquitto
  echo "Filing docker-compose"
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cat $DIR/Mosquitto.compose.yaml >> $HOME/docker-compose.yaml
#  sed -i '/services:/rMosquitto.compose.yaml' $HOME/docker-compose.yaml
  echo "Configuring mosquitto"
  echo "persistence true
persistence_location /mosquitto/data/

allow_anonymous false
password_file  /mosquitto/config/passwd

# Port to use for the default listener.
port 1883

log_dest stdout

#listener 9001
#protocol websockets " > $HOME/config/mosquitto/mosquitto.conf
echo "Securing mosquitto"
cp passwd $HOME/config/mosquitto/passwd
fi




########################
# NODE-RED
########################
read -p "Do you want to install Node-RED? [yn] " answer
if [[ $answer = y ]] ; then
  echo "Creating config folder"
  mkdir -p $HOME/config/node-red
  echo "Filing docker-compose"
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cat $DIR/Node-RED.compose.yaml >> $HOME/docker-compose.yaml
  echo "Adding volume to docker-copmose"

  if grep -q '^volumes:' $HOME/docker-compose.yaml ; then
    ## If volumes does exist
    sed -i '/^volumes:/a\  node-red-modules:' $HOME/docker-compose.yaml
  else
    ## If volumes doesn't exist
    sed -i '/services:/i\volumes:\n  node-red-modules:' $HOME/docker-compose.yaml
  fi
  echo "Node-red Installing palettes"
  docker-compose -f $HOME/docker-compose.yaml up -d node-red
  sleep 20
  docker exec -it node-red npm install \
    node-red-contrib-time-range-switch \
    node-red-contrib-home-assistant-websocket \
    node-red-contrib-counter \
    node-red-contrib-weekday \
    node-red-contrib-stoptimer \
    node-red-node-pi-gpiod
  echo "Securing node-red"
  echo "Enter password for Node-RED"
  read NodeRED_passwd
  docker exec -it node-red node -e "console.log(require('bcryptjs').hashSync(process.argv[1], 8));" $NodeRED_passwd
  echo "copy, replace hash and uncomment on $HOME/config/node-red/settings.js"
  docker-compose -f $HOME/docker-compose.yaml down
  
  echo "Installing PiGPIO"
  sudo apt install -y pigpio
  
  echo "Configuring auto-start of PiGPIO"
  if ! grep -q "/usr/bin/pigpio" /etc/rc.local; then
    sudo sed -i '/exit 0/i /usr/bin/pigpio' /etc/rc.local
  fi
fi




########################
# HOME ASSISTANT
########################
read -p "Do you want to install Home Assistant? [yn] " answer
if [[ $answer = y ]] ; then
  echo "Creating config folder"
  mkdir -p $HOME/config/homeassistant
  echo "Filing docker-compose"
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cat $DIR/HomeAssistant.compose.yaml >> $HOME/docker-compose.yaml
  if [ -d $HOME/config/homeassistant/docker/ ]; then
    rm -rfv $HOME/config/homeassistant/docker/
    git clone https://github.com/tribut/homeassistant-docker-venv $HOME/config/homeassistant/docker/
  fi
fi




########################
# DUCK DNS
########################
read -p "Do you want to install Duck DNS? [yn] " answer
if [[ $answer = y ]] ; then
  echo "Filing docker-compose"
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cat $DIR/DuckDNS.compose.yaml >> $HOME/docker-compose.yaml
fi




########################
# TASMOADMIN
########################
read -p "Do you want to install TasmoAdmin? [yn] " answer
if [[ $answer = y ]] ; then
  echo "Filing docker-compose"
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cat $DIR/Tasmoadmin.compose.yaml >> $HOME/docker-compose.yaml
  if grep -q '^volumes:' $HOME/docker-compose.yaml ; then
    ## If volumes does exist
    sed -i '/^volumes:/a\  tasmoadmin:' $HOME/docker-compose.yaml
  else
    ## If volumes doesn't exist
    sed -i '/services:/i\volumes:\n  tasmoadmin:' $HOME/docker-compose.yaml
  fi
fi


########################
# TRAEFIK
########################
read -p "Do you want to install Traefik? [yn] " answer
if [[ $answer = y ]] ; then
  echo "Creating config folder"
  mkdir -p $HOME/config/traefik
  echo "Filing docker-compose"
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cat $DIR/Traefik.compose.yaml >> $HOME/docker-compose.yaml
  echo "############################################################"
  echo "# CONFIGURING TRAEFIK"
  echo "############################################################"
  cp $DIR/traefik/* $HOME/config/traefik/.
  chmod 600 $HOME/config/traefik/acme.json
  [ ! "$(docker network ls | grep proxy)" ] && docker network create proxy
  echo "Adding network to docker-copmose"

  if grep -q '^networks:' $HOME/docker-compose.yaml ; then
    ## If networks does exist
    sed -i '/^networks:/a\  proxy:\n    external: true' $HOME/docker-compose.yaml
  else
    ## If volumes doesn't exist
    sed -i '/services:/i\networks:\n  proxy:\n    external: true' $HOME/docker-compose.yaml
  fi
fi
